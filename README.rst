
==============================================================================
collective.pfg_totalling
==============================================================================

Dynamic totalling javascript for use with PFG.



Installation
------------

Install collective.pfg_totalling by adding it to your buildout::

    [buildout]

    ...

    eggs =
        collective.pfg_totalling


and then running ``bin/buildout``


Contribute
----------

- Issue Tracker: https://github.com/collective/collective.pfg_totalling/issues
- Source Code: https://github.com/collective/collective.pfg_totalling
- Documentation: https://docs.plone.org/foo/bar


Support
-------

None!

License
-------

The project is licensed under the GPLv2.
