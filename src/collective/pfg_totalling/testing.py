# -*- coding: utf-8 -*-
from plone.app.robotframework.testing import REMOTE_LIBRARY_BUNDLE_FIXTURE
from plone.app.testing import applyProfile
from plone.app.testing import FunctionalTesting
from plone.app.testing import IntegrationTesting
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import PloneSandboxLayer
from plone.testing import z2

import collective.pfg_totalling


class CollectivePfgTotallingLayer(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        # Load any other ZCML that is required for your tests.
        # The z3c.autoinclude feature is disabled in the Plone fixture base
        # layer.
        self.loadZCML(package=collective.pfg_totalling)

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'collective.pfg_totalling:default')


COLLECTIVE_PFG_TOTALLING_FIXTURE = CollectivePfgTotallingLayer()


COLLECTIVE_PFG_TOTALLING_INTEGRATION_TESTING = IntegrationTesting(
    bases=(COLLECTIVE_PFG_TOTALLING_FIXTURE,),
    name='CollectivePfgTotallingLayer:IntegrationTesting'
)


COLLECTIVE_PFG_TOTALLING_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(COLLECTIVE_PFG_TOTALLING_FIXTURE,),
    name='CollectivePfgTotallingLayer:FunctionalTesting'
)


COLLECTIVE_PFG_TOTALLING_ACCEPTANCE_TESTING = FunctionalTesting(
    bases=(
        COLLECTIVE_PFG_TOTALLING_FIXTURE,
        REMOTE_LIBRARY_BUNDLE_FIXTURE,
        z2.ZSERVER_FIXTURE
    ),
    name='CollectivePfgTotallingLayer:AcceptanceTesting'
)
