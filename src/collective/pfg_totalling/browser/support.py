# -*- coding: utf-8 -*-

# Support for PFG form totalling; typically used with PayPal-type buttons

from Products.CMFCore.utils import getToolByName
from Products.Five import BrowserView

import re
import types


class SupportView(BrowserView):

    def total(self):
        """ total the dollar-amount fields into total_donation and amount fields """

        request = self.request
        context = self.context

        amount = 0.0
        ftypes = []
        grand_total = None
        summary = ''
        for item in context.fgFields():
            oid = item.getName()
            label = item.widget.label
            value = request.form.get(oid, '0')
            ftype = item.type
            ftypes.append(ftype)
            if value and ftype == 'boolean':
                match = re.match(r".*\$(\d+).*", label)
                if match is not None:
                    amount += int(match.group(1))
            if value and ftype == 'string':
                if isinstance(value, types.ListType):
                    # A multi-select
                    for s in value:
                        match = re.match(r".*\$([0-9.]+).*", s)
                        if match is not None:
                            fvalue = float(match.group(1))
                            if fvalue >= 0.01:
                                amount += fvalue
                else:
                    # A select
                    match = re.match(r".*\$([0-9.]+).(each|per).*", label)
                    if match is not None:
                        fvalue = float(match.group(1))
                        if fvalue >= 0.1:
                            amount += fvalue * int(value)
            if value and ftype == 'fixedpoint':
                fvalue = float(value)
                if fvalue >= 0.01:
                    amount += float(value)
                    request.form[oid] = '$%0.2f' % float(value)
            summary = "%s\n%s %s" % (summary, oid, ftype)

        if grand_total is not None:
            amount = grand_total

        request.form['amount'] = '$%0.2f' % amount
        request.form['total_donation'] = amount

    def header(self):
        """ injectable header for js/css """

        portal_url = getToolByName(self.context, 'portal_url').getPortalObject().absolute_url()

        return r"""<script src="{}/++resource++collective.pfg_totalling/total.js"></script>""".format(portal_url)
