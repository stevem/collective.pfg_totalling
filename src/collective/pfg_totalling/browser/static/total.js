jQuery(function($) {

    var my_fields = [],
        total_field = 0.0,
        total_fields,
        find_fields;

    total_fields = function() {
        var the_total = 0.0;
        // console.log('event', this);
        $.each(my_fields, function (index, value_function) {
            // console.log(value_function());
            the_total += value_function();
        });
        // console.log(the_total);
        total_field.val(the_total.toFixed(2));
    }

    find_fields = function() {
        total_field = $('div.pfg-form form #amount');
        total_field
            .attr('disabled', 'true')
            .css('font-size', "200%")
            .css('border', 'none')
            .css('background-color', '#fff')
            .css('color', '#000')
            .before("<span style='font-size:200%''>$</span>")
            .addClass('noUnloadProtection');

        $('div.pfg-form form div.field.ArchetypesDecimalWidget').each(function() {
            var jqt = $(this),
                my_input = jqt.find('input'),
                my_value;

            // console.log(jqt, my_input, my_input.val());

            my_fields.push(function () {
                my_value = my_input.val();
                if (my_value.length > 0) {
                    return parseFloat(my_input.val());
                } else {
                    return 0.0;
                }
            });

            jqt.change(total_fields);
        });
        $('div.pfg-form form div.field.ArchetypesBooleanWidget, div.pfg-form form div.field .ArchetypesMultiSelectionValue').each(function() {
            var jqt = $(this),
                my_input = jqt.find('input'),
                my_label = jqt.find('label').text(),
                my_match = my_label.match(/\$([0-9.]+)/),
                my_value = 0;

            if (my_match !== null) {
                my_value = parseFloat(my_match[1]);

                my_fields.push(function () {
                    if (my_input.attr('checked')) {
                        return my_value;
                    } else {
                        return 0.0;
                    }
                });

                jqt.change(total_fields);
            }

        });
        // $('div.pfg-form form div.field .ArchetypesMultiSelectionValue').each(function() {
        //     var jqt = $(this),
        //         my_input = jqt.find('input'),
        //         my_label = jqt.find('label').text(),
        //         my_match = my_label.match(/\$([0-9.]+)/),
        //         my_value = 0;

        //     if (my_match !== null) {
        //         my_value = parseFloat(my_match[1]);

        //         my_fields.push(function () {
        //             if (my_input.attr('checked')) {
        //                 return my_value;
        //             } else {
        //                 return 0.0;
        //             }
        //         });

        //         jqt.change(total_fields);
        //     }

        // });
        $('div.pfg-form form div.field.ArchetypesSelectionWidget').each(function() {
            var jqt = $(this),
                my_input = jqt.find('select'),
                my_label = jqt.find('label').text(),
                my_match = my_label.match(/\$([0-9.]+) each/),
                my_value = 0;

            if (my_match !== null) {
                my_value = parseFloat(my_match[1]);

                my_fields.push(function () {
                    var selected = my_input.find(':selected');
                    if (selected) {
                        return parseInt(selected.val(), 10) * my_value ;
                    } else {
                        return 0.0;
                    }
                });

                my_input.change(total_fields);
            }

        });
    }

    find_fields();
    total_fields();

});

